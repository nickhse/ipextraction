import datetime
import re

INPUT_FILE = 'access.log'
OUTPUT_FILE = 'ip_20_list.txt'
IP_REGEX = '^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
DATE_REGEX = '\[.*?\]\B'
DATE_FORMAT = '%d/%b/%Y:%H:%M:%S %z'
START_DATE = datetime.datetime(2016, 1, 28, tzinfo=datetime.timezone.utc)
END_DATE = datetime.datetime(2016, 2, 26, tzinfo=datetime.timezone.utc)

date_pattern = re.compile(DATE_REGEX)
ip_pattern = re.compile(IP_REGEX)
ips = {}
ip_count = 0

start = datetime.datetime.utcnow()
print("Ip extraction was stared")

with open(INPUT_FILE, 'r') as logfile:
    for line in logfile:
        ip_count += 1
        if line[0] != '\n':
            date_str = date_pattern.findall(line)[0][1:-1]
            date = datetime.datetime.strptime(date_str, DATE_FORMAT)
            if date > START_DATE and date < END_DATE:
                ip = ip_pattern.findall(line)[0]
                if ips.get(ip) is None:
                    ips[ip] = 1
                else:
                    ips[ip] += 1
                print(ip)

with open(OUTPUT_FILE, 'w') as output_file:
    for k, v in [(k, ips[k]) for k in sorted(ips, key=ips.get, reverse=True)][
                :20]:
        output_file.write(k + ' (' + str(v) + ')\n')

print("The first 20 ips (from {ip_count}) were saved to ip_20_list.txt file".format(ip_count=ip_count))
